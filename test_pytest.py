import pytest
from datetime import datetime
import enlighten_satellite_monitoring as esm

def test_fire_alert():
    # Clear alerts list
    esm.alerts.clear()

    # Call fire_alert with test data
    esm.fire_alert('somesatelliteid', datetime.now(), 'BATT', 'RED LOW')

    # Check if alert was added to alerts list
    assert len(esm.alerts) == 1
    assert esm.alerts[0]['satelliteId'] == 'somesatelliteid'
    assert esm.alerts[0]['component'] == 'BATT'
    assert esm.alerts[0]['severity'] == 'RED LOW'

def test_check_file():
    # Test with valid file
    try:
        esm.check_file()
    except SystemExit:
        pytest.fail("check_file failed with valid file")

    # Test with non-existent file
    esm.INPUT_FILE = "non_existent_file.txt"
    with pytest.raises(SystemExit):
        esm.check_file()
