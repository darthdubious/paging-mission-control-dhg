#!/usr/bin/env python3
import os
import sys
import json
from datetime import datetime
from collections import deque

#Constant variables related to the files on the system
INPUT_FILE = "input.txt"
OUTPUT_FILE = "alerts.json"
MAX_FILE_SIZE = 50 * 1024 * 1024  # 50MB
# A list to store alerts
alerts = []

def fire_alert(satellite_id, timestamp, component, severity):
    #Creates an alert dictionary and adds it to the alerts list.#
    alert = {
        'satelliteId': satellite_id,
        'severity': severity,
        'component': component,
        'timestamp': timestamp.isoformat()  # convert datetime to string
    }
    alerts.append(alert)

def check_alert_conditions(satellite_id, component, readings):
    #Checks if readings violate given limits and fires an alert if they do.
    if len(readings) < 3:
        return

    red_high_limit = readings[0]['red_high_limit']
    red_low_limit = readings[0]['red_low_limit']

    for reading in readings:
        if component == 'BATT' and reading['raw_value'] < red_low_limit:
            fire_alert(satellite_id, reading['timestamp'], component, "RED LOW")
        elif component == 'TSTAT' and reading['raw_value'] > red_high_limit:
            fire_alert(satellite_id, reading['timestamp'], component, "RED HIGH")

def check_file():
    #Check if input file is valid and not too large.
    if os.path.isfile(INPUT_FILE):
        INPUT_FILE_size = os.path.getsize(INPUT_FILE)
    else:
        print(f"Error: No file found at {INPUT_FILE}. Please check the file path.")
        sys.exit(1)

    if INPUT_FILE_size >= MAX_FILE_SIZE:
        print(f"Error: File {INPUT_FILE} exceeds maximum size of 50MB.")
        sys.exit(1)

def read_data():
    #Read data from input file and perform necessary checks.
    satellite_data = {}
    check_file()
    print("Starting to read data...")

    with open(INPUT_FILE, 'r') as file:
        for line in file:
            timestamp, satellite_id, red_high_limit, yellow_high_limit, yellow_low_limit, red_low_limit, raw_value, component = line.strip().split("|")
            timestamp = datetime.strptime(timestamp, '%Y%m%d %H:%M:%S.%f')
            red_high_limit = float(red_high_limit)
            red_low_limit = float(red_low_limit)
            raw_value = float(raw_value)

            if satellite_id not in satellite_data:
                satellite_data[satellite_id] = {'BATT': deque(maxlen=3), 'TSTAT': deque(maxlen=3)}

            satellite_data[satellite_id][component].append({
                'timestamp': timestamp,
                'red_high_limit': red_high_limit,
                'red_low_limit': red_low_limit,
                'raw_value': raw_value
            })

            # Check alert condition for every new entry
            check_alert_conditions(satellite_id, component, satellite_data[satellite_id][component])

    print("Finished reading data.")

def main():
    read_data()
    print("All alerts:")
    print(json.dumps(alerts, indent=2))
    with open(OUTPUT_FILE, 'w') as write_file:
        json.dump(alerts, write_file, indent=2)

if __name__ == "__main__":
    main()
